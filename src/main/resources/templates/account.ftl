<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <#include "partials/head.ftl">
    <title>My account</title>
</head>
<body>

<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">My Account</h1>


        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputName">Name</label>
                <input type="text" class="form-control" id="inputName" placeholder="Name" value="${user.name!''}"
                       name="name" readonly>
            </div>
            <div class="form-group col-md-6">
                <label for="inputSurname">Surname</label>
                <input type="text" class="form-control" id="inputSurname" placeholder="surname"
                       value="${user.surname!''}" name="surname" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputAFM">AFM</label>
                <input type="text" class="form-control" id="inputAFM" placeholder="AFM" value="${user.afm!''}"
                       name="afm" readonly>
            </div>
            <div class="form-group col-md-6">
                <label for="inputAddress">Address</label>
                <input type="text" class="form-control" id="inputAddress" placeholder="Address"
                       value="${user.address!''}" name="address" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail">Email</label>
                <input type="text" class="form-control" id="inputEmail" placeholder="Email" value="${user.email!''}"
                       name="email" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputVehicleBrand">Vehicle Brand</label>
                <input type="text" class="form-control" id="inputVehicleBrand" placeholder="Vehicle Brand"
                       value="${user.vehicleBrand!''}" name="vehicleBrand" readonly>
            </div>
            <div class="form-group col-md-6">
                <label for="inputVehiclePlate">Vehicle Plate</label>
                <input type="text" class="form-control" id="inputVehiclePlate" placeholder="Plate"
                       value="${user.vehiclePlate!''}" name="vehiclePlate" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputUserType">User Type</label>
                <select id="inputUserType" class="form-control" name="userType" readonly>
                    <option selected>${user.userType}</option>
                </select>
            </div>
        </div>
    </div>
</main>

<#include "partials/scripts.ftl">

</body>
</html>