<!DOCTYPE html>
<html lang="en">

<head>
    <#include "partials/head.ftl">
    <title>Owner information</title>
</head>

<body>

<#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">Repair Info</h1>

        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Vehicle Brand</th>
                        <th>User Plate</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Cost</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                <#list userRepairs as repair>
                    <tr>
                        <td> ${repair.date!'-'}</td>
                        <td> ${repair.user.vehicleBrand!'-'}</td>
                        <td> ${repair.user.vehiclePlate!'-'}</td>
                        <td> ${repair.type!'-'}</td>
                        <td> ${repair.status!'-'}</td>
                        <td style="text-align: right"> ${repair.cost !'-'} €</td>
                        <td>${repair.description!'-'}</td>
                    </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>

    <#include "partials/scripts.ftl">

</body>
</html>