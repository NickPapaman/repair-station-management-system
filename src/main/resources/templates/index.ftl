<!doctype html>
<html lang="en">

<head>
    <#include "partials/head.ftl">

    <title>Car Repair - Home</title>
</head>


<body>


<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">Welcome to Car Repair</h1>

        <div>
            <p style="text-align: center; font-size: x-large"><strong>Today's Repairs</strong></p>
            <table class="table">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>User's Name</th>
                    <th>Vehicle Brand</th>
                    <th>Plate</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Cost</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <#list todayRepairs as repair>
                    <tr>
                    <td> ${repair.date!'-'}</td>
                    <td> ${repair.user.surname!'-'}
                    <td> ${repair.user.vehicleBrand!'-'}</td>
                    <td> ${repair.user.vehiclePlate!'-'}</td>
                    <td> ${repair.type!'-'}</td>
                    <td> ${repair.status!'-'}</td>
                    <td style="text-align: right"> ${repair.cost !'-'} €</td>
                    <td>${repair.description!'-'}</td>
                    </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>


</main>

<#include "partials/scripts.ftl">

</body>
</html>
