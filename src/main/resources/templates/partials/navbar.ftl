    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <#if '${Session.role}' == 'ADMIN'>
            <a class="navbar-brand" href="/">Car Repair</a>
        <#else>
            <a class="navbar-brand" href="/owner">Car Repair</a>
        </#if>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav">
                <#if '${Session.role}' == 'ADMIN'>
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Users</a>
                        <ul class="dropdown-menu" style="background-color: #343a40">
                            <li><a class="nav-link" style="line-height: 0.75" href="/users/registration">Create</a></li>
                            <li><a class="nav-link" style="line-height: 0.75" href="/users/search">Search</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Repairs</a>
                        <ul class="dropdown-menu" style="background-color: #343a40">
                            <li><a class="nav-link" style="line-height: 0.75" href="/repairs/registration">Create</a>
                            </li>
                            <li><a class="nav-link" style="line-height: 0.75" href="/repairs/search">Search</a></li>
                        </ul>
                    </li>
                <#else>
                    <li class="nav-item active">
                        <a class="nav-link" href="/owner">Home <span class="sr-only">(current)</span></a>
                    </li>
                </#if>
            </ul>

            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">

                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">More options</a>
                    <ul class="dropdown-menu">
                        ...
                    </ul>
                </li>
            </ul>


            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/account">${Session.name!'-'} ${Session.surname!'-'} </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </ul>
        </div>
    </nav>
