<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">
    <title>Car Repair - Repair Update</title>

</head>

<body onload="onLoad()">
<#include "partials/navbar.ftl">

<div class="jumbotron" style="padding: 4rem 2rem">
    <h1 style="text-align: center">Repair Update</h1>

<#import "/spring.ftl" as spring />
    <form action="/repairs/update" name="registerRepairForm" method="post">

        <input type="hidden" class="form-control" width="270" name="id"
               value="${registerRepairForm.id}"/>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputDate">Date</label>
                <@spring.bind "registerRepairForm.date"/>
                <input type="date" class="form-control" width="270" placeholder="DD/MM/YYYY" name="date"
                       value="${registerRepairForm.date}"/>
                <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                </#list>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputStatus">Status</label>
                <select id="inputStatus" class="form-control" name="status">
                    <#if '${registerRepairForm.status}' == 'ON_HOLD'>
                        <option selected>ON_HOLD</option>
                        <option>ON_PROGRESS</option>
                        <option>DONE</option>
                    <#elseif '${registerRepairForm.status}' == 'ON_PROGRESS' >
                        <option>ON_HOLD</option>
                        <option selected>ON_PROGRESS</option>
                        <option>DONE</option>
                    <#else >
                        <option>ON_HOLD</option>
                        <option>ON_PROGRESS</option>
                        <option selected>DONE</option>
                    </#if>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="inputType">Type</label>
                <select id="inputType" class="form-control" name="type">
                    <#if '${registerRepairForm.type}'=='SMALL'>
                        <option selected>SMALL</option>
                        <option>HEAVY</option>
                    <#else>
                        <option>SMALL</option>
                        <option selected>HEAVY</option>
                    </#if>
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCost">Cost</label>
                <@spring.bind "registerRepairForm.cost"/>
                <input type="text" class="form-control" id="inputCost" placeholder="Cost" name="cost"
                       value="${registerRepairForm.cost}">
                <#list spring.status.errors.getFieldErrors("cost") as error>
                    <div class="alert alert-danger" role="alert">${error.defaultMessage}</div>
                </#list>
            </div>
            <div class="form-group col-md-6">
                <label for="inputUser">Owner</label>
                <@spring.bind "registerRepairForm.user"/>
                <select id="inputUser" class="form-control" name="user" placeholder="Name Surname AFM">
                    <#list users as user>
                        <option <#if '${registerRepairForm.user}'=='${user.afm}'> selected </#if>value="${user.afm}">${user.name} ${user.surname} ${user.afm}</option>
                    </#list>
                </select>
                <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                </#list>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputDescription">Description</label>
                <@spring.bind "registerRepairForm.description"/>
                <textarea class="form-control" id="inputDescription"
                          name="description" placeholder="Description"
                          rows="3">${registerRepairForm.description}</textarea>
                <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                </#list>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>

</div>



<#include "partials/scripts.ftl">

</body>
</html>
