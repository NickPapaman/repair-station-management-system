-- https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-sql.html
-- https://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html
-- IN THIS FILE WE CAN WRITE AN SQL SCRIPT CONTAINING:
-- SCHEMA, TABLE AND DATA MANIPULATION QUERIES
-- TO BE EXECUTED AUTOMATICALLY DURING THE INITIALIZATION OF THE APPLICATION
-- AND AFTER THE CREATION OF SCHEMA AND TABLES BY Hibernate
-- IF spring.jpa.hibernate.ddl-auto IS SET TO create OR create-drop
-- IT IS A Hibernate feature (nothing to do with Spring)a

--encrypted passwords of "0000" for every user

insert into USER(Afm, Name, Surname, Address,Email, Password, Vehicle_Brand, Vehicle_Plate, User_Type) values ( '123456789', 'Antonis', 'Korompos','Hrakleio', 'a@gmail.com', '$2a$10$gc6iFEXguM0.Qx4FW7Z5fu62TUtOb5qhqZu07KaXhbAEkEuYqZVCy', 'Toyota', 'abc1234' ,0);

insert into USER(Afm, Name, Surname, Address,Email, Password, Vehicle_Brand, Vehicle_Plate, User_Type) values ( '987654321', 'Nikos', 'Kalantas','Aigaleo', 'b@gmail.com', '$2a$10$gc6iFEXguM0.Qx4FW7Z5fu62TUtOb5qhqZu07KaXhbAEkEuYqZVCy', 'Nissan', 'dsg7762' ,1);

insert into USER(Afm, Name, Surname, Address, Email, Password, Vehicle_Brand, Vehicle_Plate, User_Type) values ( '123456000', 'Giorgos', 'Matsamplokos', 'Peristeri', 'c@gmail.com', '$2a$10$gc6iFEXguM0.Qx4FW7Z5fu62TUtOb5qhqZu07KaXhbAEkEuYqZVCy', 'Nissan', 'dfg7762' ,1);

insert into USER(Afm, Name, Surname, Address, Email, Password, Vehicle_Brand, Vehicle_Plate, User_Type) values ( '789456000', 'Paraskevas', 'Koutsikos', 'Methana', 'd@gmail.com', '$2a$10$gc6iFEXguM0.Qx4FW7Z5fu62TUtOb5qhqZu07KaXhbAEkEuYqZVCy', 'Nissan', 'dsl9362' ,0);

insert into USER(Afm, Name, Surname, Address, Email, Password, Vehicle_Brand, Vehicle_Plate, User_Type) values ( '567890000', 'Kostas', 'Troufas', 'Koridallos', 'e@gmail.com', '$2a$10$gc6iFEXguM0.Qx4FW7Z5fu62TUtOb5qhqZu07KaXhbAEkEuYqZVCy', 'Datsun', 'dsk0762' ,0);

insert into USER(Afm, Name, Surname, Address, Email, Password, Vehicle_Brand, Vehicle_Plate, User_Type) values ( '678900000', 'Manolis', 'Kavourdiris', 'Nea Smurni', 'f@gmail.com', '$2a$10$gc6iFEXguM0.Qx4FW7Z5fu62TUtOb5qhqZu07KaXhbAEkEuYqZVCy', 'Suzuki', 'dhu8762' ,0);

insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (400, '2018-12-14', 'New tires', 2, 1, 1)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (150, '2018-12-15', 'Exhaust fix', 2, 1, 2)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (50, '2018-12-16', 'Oil changes', 2, 0, 3)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (400, '2018-12-17', 'New tires', 2, 1, 4)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (150, '2018-12-18', 'Exhaust fix', 2, 1, 1)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (50, '2018-12-19', 'Oil changes', 2, 0, 2)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (400, '2018-12-20', 'New tires', 1, 1, 3)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (150, '2018-12-20', 'Exhaust fix', 0, 1, 4)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (50, '2018-12-20', 'Oil changes', 2, 0, 1)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (400, '2018-12-20', 'New tires', 1, 1, 2)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (150, '2018-12-20', 'Exhaust fix', 1, 0, 3)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (50, '2018-12-20', 'Oil changes', 0, 0, 4)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (400, '2018-12-20', 'New tires', 2, 1, 5)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (150, '2018-12-20', 'Exhaust fix', 1, 1, 6)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (50, '2018-12-21', 'Oil changes', 0, 0, 2)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (50, '2018-12-22', 'Oil changes', 0, 1, 3)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (400, '2018-12-23', 'New tires', 0, 1, 6)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (150, '2018-12-24', 'Exhaust fix', 0, 1, 4)
insert into REPAIR(Cost, Date, Description, Status, Type, User_Id) values (50, '2018-12-25', 'Oil changes', 0, 0, 2)