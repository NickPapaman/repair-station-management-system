package gr.project.future.team1.exception;

import gr.project.future.team1.utils.GlobalAttributes;

public class UserEmailExistException extends RuntimeException {
    public UserEmailExistException() {
        super(GlobalAttributes.USER_MAIL_EXISTS);
    }

}
