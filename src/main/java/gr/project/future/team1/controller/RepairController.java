package gr.project.future.team1.controller;

import gr.project.future.team1.controller.mapper.RepairToRegisterRepairMapper;
import gr.project.future.team1.domain.Repair;
import gr.project.future.team1.form.RegisterRepairForm;
import gr.project.future.team1.form.RepairSearchForm;
import gr.project.future.team1.model.UserModel;
import gr.project.future.team1.service.RepairService;
import gr.project.future.team1.service.UserServiceImpl;
import gr.project.future.team1.validator.RegisterRepairFormValidator;
import gr.project.future.team1.validator.RepairSearchFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class RepairController {

    @Autowired
    RepairService repairService;

    @Autowired
    UserServiceImpl userServiceImpl;

    @Autowired
    RepairSearchFormValidator repairSearchFormValidator;

    @Autowired
    RegisterRepairFormValidator registerRepairFormValidator;

    @Autowired
    RepairToRegisterRepairMapper repairToRegisterRepairMapper;


    @InitBinder("repairSearchForm")
    protected void initSearchBinder(WebDataBinder webDataBinder){
        webDataBinder.addValidators(repairSearchFormValidator);
    }

    @InitBinder("registerRepairForm")
    protected void initRegisterBinder(WebDataBinder webDataBinder){
        webDataBinder.addValidators(registerRepairFormValidator);
    }

    @GetMapping(value = "/repairs/info")
    public String repairInfo(Model model) {
        return "repair-info";
    }

    @GetMapping(value = "/repairs/update/{id}")
    public String repairUpdate(Model model, @PathVariable(name = "id") long updateId) {
        Repair repair = repairService.findById(updateId);
        model.addAttribute("registerRepairForm", repairToRegisterRepairMapper.mapRepairToRegisterRepairForm(repair));
        List<UserModel> users = userServiceImpl.findAll();
        model.addAttribute("users", users);
        return "repair-update";
    }

    @RequestMapping(value = "/repairs/update", method = RequestMethod.POST)
    public String repairsUpdate(Model model,
                                @Valid @ModelAttribute("registerRepairForm") RegisterRepairForm registerRepairForm,
                                BindingResult bindingResult,
                                HttpSession session,
                                RedirectAttributes redirectAttributes) {
        if(bindingResult.hasErrors()){
            List<UserModel> users = userServiceImpl.findAll();
            model.addAttribute("users", users);
            return "repair-update";
        }
        repairService.create(registerRepairForm);
        List<Repair> repairs = repairService.findByCriteria((RepairSearchForm) session.getAttribute("repairSearchForm"));
        redirectAttributes.addFlashAttribute("repairs", repairs);
        return "redirect:/repairs/info";
    }

    @GetMapping(value = "/repairs/search")
    public String userSearch(Model model, @ModelAttribute("errorMessage") final String errorMessage) {
        if(!model.containsAttribute("repairSearchForm")) {
            model.addAttribute("repairSearchForm", new RepairSearchForm());
        }
        return "repair-search";
    }

    @RequestMapping(value = "/repairs/search", method = RequestMethod.POST)
    public String repairsSearch(Model model, @Valid @ModelAttribute("repairSearchForm") RepairSearchForm repairSearchForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession session) {
        if(bindingResult.hasErrors()){
            return "repair-search";
        }
        List<Repair> repairs = repairService.findByCriteria(repairSearchForm);
        redirectAttributes.addFlashAttribute("repairs", repairs);
        session.setAttribute("repairSearchForm", repairSearchForm);
        return "redirect:/repairs/info";
    }

    @RequestMapping(value = "/repairs/delete/{id}")
    public String repairsDelete(Model model, @PathVariable(name = "id") long deleteId, HttpSession session, RedirectAttributes redirectAttributes) {
        repairService.deleteById(deleteId);
        List<Repair> repairs = repairService.findByCriteria((RepairSearchForm) session.getAttribute("repairSearchForm"));
        redirectAttributes.addFlashAttribute("repairs", repairs);
        return "redirect:/repairs/info";
    }


}
