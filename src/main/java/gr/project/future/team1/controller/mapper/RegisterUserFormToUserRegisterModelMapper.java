package gr.project.future.team1.controller.mapper;

import gr.project.future.team1.form.RegisterUserForm;
import gr.project.future.team1.model.UserRegisterModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class RegisterUserFormToUserRegisterModelMapper {

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserRegisterModel mapToUserModel(RegisterUserForm registerUserForm){
        UserRegisterModel userRegisterModel = new UserRegisterModel();
        userRegisterModel.setName(registerUserForm.getName());
        userRegisterModel.setSurname(registerUserForm.getSurname());
        userRegisterModel.setAfm(registerUserForm.getAfm());
        userRegisterModel.setEmail(registerUserForm.getEmail());
        userRegisterModel.setUserType(registerUserForm.getUserType());
        userRegisterModel.setAddress(registerUserForm.getAddress());
        userRegisterModel.setVehiclePlate(registerUserForm.getVehiclePlate());
        userRegisterModel.setVehicleBrand(registerUserForm.getVehicleBrand());
        userRegisterModel.setPassword(passwordEncoder.encode(registerUserForm.getPassword()));
        return userRegisterModel;
    }
}
