package gr.project.future.team1.controller.mapper;


import gr.project.future.team1.domain.User;
import gr.project.future.team1.model.UserModel;
import gr.project.future.team1.model.UserRegisterModel;
import org.springframework.stereotype.Component;

@Component
public class UserToUserModelMapper {
    public UserModel mapToUserModel(User user){
        UserModel userModel = new UserModel();
        userModel.setId(user.getId());
        userModel.setName(user.getName());
        userModel.setSurname(user.getSurname());
        userModel.setAddress(user.getAddress());
        userModel.setAfm(user.getAfm());
        userModel.setUserType(user.getUserType());
        userModel.setEmail(user.getEmail());
        userModel.setVehicleBrand(user.getVehicleBrand());
        userModel.setVehiclePlate(user.getVehiclePlate());
        return userModel;
    }
}
