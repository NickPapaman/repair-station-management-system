package gr.project.future.team1.controller.mapper;

import gr.project.future.team1.domain.Repair;
import gr.project.future.team1.form.RegisterRepairForm;
import org.springframework.stereotype.Component;

@Component
public class RepairToRegisterRepairMapper {
    public RegisterRepairForm mapRepairToRegisterRepairForm(Repair repair){
        RegisterRepairForm form= new RegisterRepairForm();
        form.setId(repair.getId());
        form.setCost(String.valueOf(repair.getCost()));
        form.setDate(repair.getDate());
        form.setDescription(repair.getDescription());
        form.setStatus(repair.getStatus());
        form.setType(repair.getType());
        form.setUser(repair.getUser().getAfm());
        return form;
    }
}
