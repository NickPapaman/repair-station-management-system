package gr.project.future.team1.controller;

import gr.project.future.team1.controller.mapper.UserToUserRegisterFormMapper;
import gr.project.future.team1.domain.User;
import gr.project.future.team1.exception.UserAfmAndEmailExistsException;
import gr.project.future.team1.exception.UserAfmExistsException;
import gr.project.future.team1.exception.UserEmailExistException;
import gr.project.future.team1.exception.UserNotFoundException;
import gr.project.future.team1.form.RegisterUserForm;
import gr.project.future.team1.form.UserSearchForm;
import gr.project.future.team1.service.UserService;
import gr.project.future.team1.utils.GlobalAttributes;
import gr.project.future.team1.validator.UserSearchFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    UserToUserRegisterFormMapper userToRegisterUserFormMapper;
    @Autowired
    UserSearchFormValidator userSearchFormValidator;

    @InitBinder("userSearchForm")
    protected void initBinder(final WebDataBinder webDataBinder){
        webDataBinder.addValidators(userSearchFormValidator);
    }
    // USER SEARCH CONTROLLERS
    //-------------------------
    @GetMapping(value = "/users/info")
    public String userInfo(@ModelAttribute("user") final User user,
                           Model model) {
        return "user-info";
    }

    @GetMapping(value = "/users/search")
    public String userSearch(Model model, @ModelAttribute("errorMessage") final String errorMessage) {
        if(!model.containsAttribute("userSearchForm")) {
            model.addAttribute("userSearchForm", new UserSearchForm());
        }
        return "user-search";
    }

    @RequestMapping(value = "/users/search",method = RequestMethod.POST)
    public String userSearch(Model model, @Valid @ModelAttribute("userSearchForm") UserSearchForm userSearchForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession session) {
        if(bindingResult.hasErrors()){
            return "user-search";
        }
        List<User> users = userService.getUser(userSearchForm.getAfm(), userSearchForm.getEmail()); // Get the correct user from the DB
        // Pass values to user-info page
        redirectAttributes.addFlashAttribute("users", users);
        session.setAttribute("userSearchForm", userSearchForm);
        return "redirect:/users/info";
    }


    // USER UPDATE CONTROLLERS
    //-------------------------
    @GetMapping(value = "/users/update/{id}")
    public String search(@PathVariable(name = "id") long id, Model model) {
        User user = userService.getUserById(id);
        RegisterUserForm form = userToRegisterUserFormMapper.mapToUserRegisterForm(user);
        model.addAttribute("registerUserForm", form);
        return "user-update";
    }


    @RequestMapping(value = "/users/update", method = RequestMethod.POST)
    public String userSearch(Model model,@Valid @ModelAttribute("registerUserForm") RegisterUserForm form,BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession session) {
        if(bindingResult.hasErrors()){
            return"user-update";
        }
        try {
            userService.userUpdate(form);
        }
        catch (UserAfmExistsException|UserAfmAndEmailExistsException|UserEmailExistException e){
            model.addAttribute("errorMessage",e.getMessage() );
            return"user-update";
        }
        UserSearchForm userSearchForm = (UserSearchForm) session.getAttribute("userSearchForm");
        List<User> users = userService.getUser(userSearchForm.getAfm(), userSearchForm.getEmail()); // Get the correct user from the DB
        // Pass values to user-info page
        redirectAttributes.addFlashAttribute("users", users);
        return "redirect:/users/info";
    }


    // USER DELETE CONTROLLERS
    //-------------------------
    @GetMapping(value = "/users/delete/{id}")
    public String userDelete(Model model, @PathVariable long id, RedirectAttributes redirectAttributes, HttpSession session) {
        userService.deleteUser(id);

        UserSearchForm userSearchForm = (UserSearchForm) session.getAttribute("userSearchForm");
        List<User> users = userService.getUser(userSearchForm.getAfm(), userSearchForm.getEmail()); // Get the correct user from the DB
        // Pass values to user-info page
        redirectAttributes.addFlashAttribute("users", users);
        return "redirect:/users/info";
    }


    // EXCEPTION HANLDERS
    // ------------------
    @ExceptionHandler(UserNotFoundException.class)
    public String handleError(HttpServletRequest request, RedirectAttributes redirectAttrs, RuntimeException e) {
        redirectAttrs.addFlashAttribute("errorMessage", "User does not exist!");
        return "redirect:/users/search";
    }


}
