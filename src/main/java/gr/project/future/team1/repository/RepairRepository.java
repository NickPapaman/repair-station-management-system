package gr.project.future.team1.repository;

import gr.project.future.team1.domain.Repair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RepairRepository extends JpaRepository<Repair,Long> {

    List<Repair> findAll();
    List<Repair> findByDate(LocalDate today);
    Repair save(Repair repair);
    Repair findById(long repairId);
    void deleteById(long repairId);
}
