package gr.project.future.team1.enums;

public enum UserType {
    OWNER,
    ADMIN
}
