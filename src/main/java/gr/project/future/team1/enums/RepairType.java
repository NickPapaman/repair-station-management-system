package gr.project.future.team1.enums;

public enum RepairType {
    SMALL,
    HEAVY
}
