package gr.project.future.team1.enums;

public enum RepairStatus {
    ON_HOLD,        // Σε αναμονή
    ON_PROGRESS,    // Σε εξέλιξη
    DONE            // Ολοκληρωμένη
}
