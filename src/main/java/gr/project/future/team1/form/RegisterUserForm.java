package gr.project.future.team1.form;

import gr.project.future.team1.enums.UserType;
import gr.project.future.team1.utils.GlobalAttributes;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RegisterUserForm {

    private long id;

    @Pattern(regexp = GlobalAttributes.AFM_PATTERN, message = GlobalAttributes.AFM_PATTERN_MESSAGE)
    @Size(min = GlobalAttributes.AFM_LENGTH, max = GlobalAttributes.AFM_LENGTH, message = GlobalAttributes.AFM_LENGTH_MESSAGE)
    private String afm;

    @Pattern(regexp = GlobalAttributes.NAME_SURNAME_PATTERN, message = GlobalAttributes.NAME_SURNAME_PATTERN_MESSAGE)
    @Size(min = GlobalAttributes.NAME_SURNAME_SIZE, message = GlobalAttributes.NAME_SURNAME_SIZE_MESSAGE)
    private String name;

    @Pattern(regexp = GlobalAttributes.NAME_SURNAME_PATTERN, message = GlobalAttributes.NAME_SURNAME_PATTERN_MESSAGE)
    @Size(min = GlobalAttributes.NAME_SURNAME_SIZE, message = GlobalAttributes.NAME_SURNAME_SIZE_MESSAGE)
    private String surname;

    @Size(min = GlobalAttributes.ADDRESS_SIZE, message = GlobalAttributes.ADDRESS_SIZE_MESSAGE)
    private String address;

    @Pattern(regexp = GlobalAttributes.MAIL_PATTERN, message = GlobalAttributes.MAIL_PATTERN_MESSAGE)
    private String email;

    @Size(min = GlobalAttributes.PASSWORD_SIZE, message = GlobalAttributes.PASSWORD_SIZE_MESSAGE)
    private String password;

    @Pattern(regexp = GlobalAttributes.BRAND_PATTERN, message = GlobalAttributes.BRAND_PATTERN_MESSAGE)
    @Size(min = GlobalAttributes.BRAND_SIZE, message = GlobalAttributes.BRAND_SIZE_MESSAGE)
    private String vehicleBrand;

    @Pattern(regexp = GlobalAttributes.PLATE_PATTERN, message = GlobalAttributes.PLATE_PATTERN_MESSAGE)
    private String vehiclePlate;
    private UserType userType;


    public String getAfm() {
        return afm;
    }

    public void setAfm(String afm) {
        this.afm = afm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVehicleBrand() {
        return vehicleBrand;
    }

    public void setVehicleBrand(String vehicleBrand) {
        this.vehicleBrand = vehicleBrand;
    }

    public String getVehiclePlate() {
        return vehiclePlate;
    }

    public void setVehiclePlate(String vehiclePlate) {
        this.vehiclePlate = vehiclePlate;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
