package gr.project.future.team1.form;

import gr.project.future.team1.utils.GlobalAttributes;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class RepairSearchForm {


    @DateTimeFormat(pattern = GlobalAttributes.DATE_PATTERN)
    private LocalDate dateFrom;

    @DateTimeFormat(pattern = GlobalAttributes.DATE_PATTERN)
    private LocalDate dateTo;

    private String afm;

    private String vehiclePlate;

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public String getAfm() {
        return afm;
    }

    public void setAfm(String afm) {
        this.afm = afm;
    }

    public String getVehiclePlate() {
        return vehiclePlate;
    }

    public void setVehiclePlate(String vehiclePlate) {
        this.vehiclePlate = vehiclePlate;
    }

    public boolean allDatesFrom() {
        return (this.getDateFrom() != null && this.getDateTo() == null);
    }

    public boolean allDatesTo() {
        return (this.getDateFrom() == null && this.getDateTo() != null);
    }

    public boolean allDatesBetween() {
        return (this.getDateFrom() != null && this.getDateTo() != null);
    }

}
