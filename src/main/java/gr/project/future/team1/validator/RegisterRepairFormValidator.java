package gr.project.future.team1.validator;

import gr.project.future.team1.form.RegisterRepairForm;
import gr.project.future.team1.utils.GlobalAttributes;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
@Component
public class RegisterRepairFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return RegisterRepairForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegisterRepairForm registerRepairForm = (RegisterRepairForm) target;
        try{
            long cost = Long.parseLong(registerRepairForm.getCost());
        }catch(Exception e){
            errors.rejectValue("cost","cost error", GlobalAttributes.COST_MESSAGE);
        }
    }
}
