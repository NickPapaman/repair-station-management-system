package gr.project.future.team1.model;

import gr.project.future.team1.enums.RepairStatus;
import gr.project.future.team1.enums.RepairType;

import java.time.LocalDate;

public class RepairModel {
    private LocalDate date;
    private RepairStatus status;
    private RepairType type;
    private long cost;
    private String description;
    private UserModel userModel;

    public RepairModel(LocalDate date, RepairStatus status, RepairType type, long cost, String description, UserModel userModel) {
        this.date = date;
        this.status = status;
        this.type = type;
        this.cost = cost;
        this.description = description;
        this.userModel = userModel;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public RepairStatus getStatus() {
        return status;
    }

    public void setStatus(RepairStatus status) {
        this.status = status;
    }

    public RepairType getType() {
        return type;
    }

    public void setType(RepairType type) {
        this.type = type;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }
}
