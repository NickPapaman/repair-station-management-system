package gr.project.future.team1.service;

import gr.project.future.team1.domain.Repair;
import gr.project.future.team1.form.RegisterRepairForm;
import gr.project.future.team1.form.RepairSearchForm;

import java.util.List;

public interface RepairService {

    List<Repair> findAll();

    Repair findById(long repairId);
    List<Repair> findByCriteria(RepairSearchForm repairSearchForm);
    void deleteById(long repairId);
    void create(RegisterRepairForm registerRepairForm);
    List<Repair> getRepairsByUserEmail(String email);
    List<Repair> getTodayRepairs();

}
