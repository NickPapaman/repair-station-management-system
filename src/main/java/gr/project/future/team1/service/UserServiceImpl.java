package gr.project.future.team1.service;


import gr.project.future.team1.controller.mapper.UserToUserModelMapper;
import gr.project.future.team1.domain.User;
import gr.project.future.team1.exception.UserAfmAndEmailExistsException;
import gr.project.future.team1.exception.UserAfmExistsException;
import gr.project.future.team1.exception.UserEmailExistException;
import gr.project.future.team1.exception.UserNotFoundException;
import gr.project.future.team1.form.RegisterUserForm;
import gr.project.future.team1.model.UserModel;
import gr.project.future.team1.model.UserRegisterModel;
import gr.project.future.team1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserToUserModelMapper userToUserModelMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public List<User> getUser(String afm, String email) throws UserNotFoundException {
        List<User> users = new ArrayList();
        if (!afm.isBlank() && !email.isBlank()) {
            users.add(userRepository.findByAfmAndEmail(afm, email));
        } else if (!afm.isBlank() || !email.isBlank()) {
            users.add(userRepository.findByAfmOrEmail(afm, email));
        } else {
            users = userRepository.findAll();
        }
        if (users.get(0) == null) {
            throw new UserNotFoundException();
        }
        return users;
    }

    public User getUserById(long id) {
        User user = userRepository.findById(id);
        return user;
    }

    @Override
    public List<UserModel> findAll() {
        return userRepository.findAll()
                .stream()
                .map(user -> userToUserModelMapper.mapToUserModel(user))
                .collect(Collectors.toList());
    }

    @Override
    public void create(UserRegisterModel userRegisterModel) {
        User user = new User(userRegisterModel.getAfm(), userRegisterModel.getName(), userRegisterModel.getSurname(), userRegisterModel.getAddress()
                , userRegisterModel.getEmail(), userRegisterModel.getPassword(), userRegisterModel.getVehicleBrand(), userRegisterModel.getVehiclePlate()
                , userRegisterModel.getUserType());
        User flagUser=userRepository.findByAfmOrEmail(userRegisterModel.getAfm(),userRegisterModel.getEmail());
        if(flagUser!=null){
            if(user.getAfm().equals(flagUser.getAfm())){
                throw (new UserAfmExistsException());
            }else{
                throw (new UserEmailExistException());
            }
        }else{
            userRepository.save(user);
        }
    }

    @Override
    public void userUpdate(RegisterUserForm form) {
        User updatedUser = new User(form.getAfm(), form.getName(), form.getSurname(), form.getAddress()
                , form.getEmail(), passwordEncoder.encode(form.getPassword()), form.getVehicleBrand(), form.getVehiclePlate()
                , form.getUserType());
        updatedUser.setId(form.getId());
        List<User> existingUsers = userRepository.findAllByAfmOrEmail(form.getAfm(),form.getEmail());
        for(User user:existingUsers){
            if(user.getAfm().equals(form.getAfm()) && user.getEmail().equals(form.getEmail()) && user.getId()!= form.getId()){
                throw new UserAfmAndEmailExistsException();
            }
            if(user.getAfm().equals(form.getAfm()) && user.getId()!= form.getId()){
                throw new UserAfmExistsException();
            }
            if(user.getEmail().equals(form.getEmail()) && user.getId() != form.getId()){
                throw new UserEmailExistException();
            }
        }
        userRepository.save(updatedUser);
    }

    @Override
    public void deleteUser(long id) {
        userRepository.deleteById(id);
    }

}
